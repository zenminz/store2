package GUI;

import java.awt.*; 
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.*;


/**
 * The class builds the main form with buttons, labels, and panels.
 * @author Peter
 *
 */
public class MainForm {
	
	//Creates the JFrame, Jbutton, and Jlabel object.
	static JFrame mainForm = new JFrame("Tiệm Điện A Cón");
	static JButton itemPropertiesBtn = new JButton("Load items");
	static JButton exportManifestsBtn = new JButton("Export manifests");
	static JButton loadManifestsBtn = new JButton("Load manifests");
	static JButton inventoryBtn = new JButton("Inventory");
	static JButton salesLogBtn = new JButton("Load sales");
	private static final JButton btnNewButton = new JButton("New button");
	


	

	/**
	 * This method intialising the main form and all its variables and components
	 * @wbp.parser.entryPoint
	 */
	public static void mainForm() {
		
		//Frame setting
		mainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainForm.setLocation(new Point(550,150));
		mainForm.setResizable(false);
		mainForm.setSize(500, 500);

		
		//Setting up panel for the frame
		JPanel mainPanel = new JPanel();
		mainPanel.setBackground(SystemColor.textHighlight);
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		gbl_mainPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		mainPanel.setLayout(gbl_mainPanel);
		mainForm.getContentPane().add(mainPanel);
		
			
		//Supermart label
		JLabel superMartLbl = new JLabel("Tiệm Điện A Cón");
		superMartLbl.setForeground(Color.CYAN);
		superMartLbl.setFont(new Font("Bookman Old Style", Font.PLAIN, 46));
			
		
		GridBagConstraints gbc_superMartLbl = new GridBagConstraints();
		gbc_superMartLbl.gridwidth = 5;
		gbc_superMartLbl.insets = new Insets(0, 0, 5, 0);
		gbc_superMartLbl.gridx = 0;
		gbc_superMartLbl.gridy = 0;
		mainPanel.add(superMartLbl, gbc_superMartLbl);
		
		//Capital label & properties
		GridBagConstraints gbc_capitalLbl = new GridBagConstraints();
		gbc_capitalLbl.anchor = GridBagConstraints.EAST;
		gbc_capitalLbl.gridx = 1;
		gbc_capitalLbl.gridy = 1;
		gbc_capitalLbl.insets = new Insets(0, 0, 5, 5);
		JLabel capitalLbl = new JLabel("Capital:");
		capitalLbl.setForeground(Color.WHITE);
		capitalLbl.setFont(new Font("Calibri", Font.ITALIC, 25));
		mainPanel.add(capitalLbl, gbc_capitalLbl);
		
		//Capital value label & properties
		GridBagConstraints gbc_capitalValueLbl = new GridBagConstraints();
		gbc_capitalValueLbl.gridwidth = 3;
		gbc_capitalValueLbl.anchor = GridBagConstraints.WEST;
		gbc_capitalValueLbl.insets = new Insets(0, 0, 5, 5);
		gbc_capitalValueLbl.gridx = 2;
		gbc_capitalValueLbl.gridy = 1;
		
		//Inventory button properties
		GridBagConstraints gbc_inventoryBtn = new GridBagConstraints();
		gbc_inventoryBtn.gridwidth = 2;
		gbc_inventoryBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_inventoryBtn.insets = new Insets(0, 0, 5, 5);
		gbc_inventoryBtn.gridx = 1;
		gbc_inventoryBtn.gridy = 2;
		mainPanel.add(inventoryBtn, gbc_inventoryBtn);
		
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 4;
		gbc_btnNewButton.gridy = 2;
		mainPanel.add(btnNewButton, gbc_btnNewButton);
		

		//Export manifest button properties
		GridBagConstraints gbc_exportManifestsBtn = new GridBagConstraints();
		gbc_exportManifestsBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_exportManifestsBtn.insets = new Insets(0, 0, 5, 5);
		gbc_exportManifestsBtn.gridx = 1;
		gbc_exportManifestsBtn.gridy = 3;
		mainPanel.add(exportManifestsBtn, gbc_exportManifestsBtn);
		
		//Load manifest button properties
		GridBagConstraints gbc_loadManifestsBtn = new GridBagConstraints();
		gbc_loadManifestsBtn.insets = new Insets(0, 0, 5, 5);
		gbc_loadManifestsBtn.gridx = 2;
		gbc_loadManifestsBtn.gridy = 3;
		mainPanel.add(loadManifestsBtn, gbc_loadManifestsBtn);
		
		//Load item properties button properties	
		//itemPropertiesBtn.setBounds(100, 113, 133, 23);
		GridBagConstraints gbc_itemPropertiesBtn = new GridBagConstraints();
		gbc_itemPropertiesBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_itemPropertiesBtn.insets = new Insets(0, 0, 0, 5);
		gbc_itemPropertiesBtn.gridx = 1;
		gbc_itemPropertiesBtn.gridy = 4;
		mainPanel.add(itemPropertiesBtn, gbc_itemPropertiesBtn);
		
		//Load sales button properties
		GridBagConstraints gbc_salesLogBtn = new GridBagConstraints();
		gbc_salesLogBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_salesLogBtn.insets = new Insets(0, 0, 0, 5);
		gbc_salesLogBtn.gridx = 2;
		gbc_salesLogBtn.gridy = 4;
		mainPanel.add(salesLogBtn, gbc_salesLogBtn);
		

		
		//Finalises the components within the JFrame and sets visibility
		mainForm.pack();
		mainForm.setVisible(true); //Displaying the frame
		

			
	}
	
	/**
	 * This method sets the mainform's visibility
	 * @param flag to indicate the mainfom's visibility
	 */
	public static void mainFormVisible(boolean flag) {
		mainForm.setVisible(flag);
	}
	
	/**
	 * This method updates the  capital label's value to a specified amount
	 * @param capital
	 */
	public static void updateCapitalLbl(double capital) {
		NumberFormat dFormat = NumberFormat.getCurrencyInstance(Locale.US);
	}
	

	public static void main(String[] args) {
	}


}
