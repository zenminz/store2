package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class addForm extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton backBtn;
	private JButton addBtn;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel btnPanel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addForm frame = new addForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the frame.
	 */
	public addForm() {
		setBounds(100, 100, 912, 576);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(10, 11, 876, 451);
		contentPane.add(panel);
		panel.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 876, 451);
		panel.add(scrollPane);
		

		
		String[] columnNames3 = {"Tên", "Số Lượng", "ĐVT", "Giá", "CK"};
		Object[][] data = new Object[10][5];

		
		DefaultTableModel addTableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			String[] columnNames3 = {"Ten", "So Luong", "ĐVT", "Gia", "CK"};
			
		    @Override
		    public int getColumnCount() {
		         return columnNames3.length;
		    }

		    @Override
		    public String getColumnName(int index) {
		        return columnNames3[index];
		    }
			
		};
		table = new JTable(data, columnNames3);
		table.setFont(new Font("SansSerif", Font.PLAIN , 20));
		table.getTableHeader().setFont(new Font("SansSerif", Font.ITALIC, 25));
		table.setRowHeight(50);
		scrollPane.setViewportView(table);
		
		btnPanel = new JPanel();
		btnPanel.setBounds(0, 901, 1100, 72);
		contentPane.add(btnPanel);
		btnPanel.setLayout(null);
		
		//btnNewButton = new JButton("New button");

		backBtn = new JButton("Quay Lại");
		backBtn.setBounds(0, 0, 435, 72);
		btnPanel.add(backBtn);
		
		addBtn = new JButton("Thêm");
		addBtn.setBounds(665, 0, 435, 72);
		btnPanel.add(addBtn);
	}
	
	/**
	 * 
	 */
	public JButton backBtn() {
		return backBtn;
		
	}
	
	public JButton addBtn() {
		return addBtn;
	}
	public JTable table() {
		return table;
		
	}
	
	public JPanel contentPane() {
		return contentPane;
	}
	
	public JPanel panel() {
		return panel;
	}
	
	public JPanel btnPanel() {
		return btnPanel;
	}
	
	public JScrollPane scrollPane() {
		return scrollPane;
	}
}
