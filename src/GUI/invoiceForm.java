package GUI;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.List;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import org.apache.poi.hpsf.Decimal;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.docx4j.model.structure.MarginsWellKnown;
import org.docx4j.model.structure.PageDimensions;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.model.table.TableModel;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblGrid;
import org.docx4j.wml.TblGridCol;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.Tr;
import org.docx4j.*;
import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.jaxb.Context;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTSignedTwipsMeasure;
import org.docx4j.wml.CTTblCellMar;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.Color;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase.Spacing;
import org.docx4j.wml.PPrBase.TextAlignment;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.SectPr.PgMar;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;


import Exception.CSVFormatException;
import Exception.DeliveryException;
import Exception.StockException;
import items.item;
import items.items;
import parser.csvParser;
import parser.docxCreator;
import parser.htmlCreator;
import parser.itemPropertiesCSV;

public class invoiceForm {

	private JFrame frame;
	private JTextField textField;
	private JTable table;
	private JTable table_2;
	private static addForm addForm;
	private static invoiceForm window;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addForm = new addForm();
					window = new invoiceForm();	

					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the application.
	 */
	public invoiceForm() {
		initialize();
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {	
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenHeight = screenSize.height;
		int screenWidth = screenSize.width;
		
		
		frame = new JFrame();
		//frame.setBounds(100, 100, 803, 533);
		frame.setSize(screenWidth,screenHeight);
		
		
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		//JPanel addPanel = (JPanel) addForm.getContentPane();
		
		
		JPanel addPanel = addForm.contentPane();
		//addPanel.setBounds(10, 87, 1100, 973);
		addPanel.setBounds((int)(0.005 * screenWidth), (int)(0.005*2*screenHeight) + (int)(0.06*screenHeight), (int)(0.55 * screenWidth), (int)((1 - (0.005 * 3 + 0.06))*screenHeight));
		//addForm.panel().setBounds(0, 0, 1100, 890);
		//addForm.scrollPane().setBounds(0, 0, 1100, 890);	
		addForm.panel().setBounds(0, 0, (int)(0.55 * screenWidth), (int)(0.86 * screenHeight));
		addForm.scrollPane().setBounds(0, 0, (int)(0.55 * screenWidth), (int)(0.86 * screenHeight));
		//addForm.btnPanel().setBounds(0, 901, 1100, 72);
		addForm.btnPanel().setBounds(0, (int)((0.86 + 0.005) * screenHeight),  (int)(0.55 * screenWidth), (int)(0.06*screenHeight));
		addForm.addBtn().setBounds(0, 0,  (int)(0.2 * screenWidth), (int)(0.06*screenHeight));
		addForm.backBtn().setBounds((int)(0.35 * screenWidth), 0,  (int)(0.2 * screenWidth), (int)(0.06*screenHeight));
		addForm.table().setFont(new Font("SansSerif", Font.PLAIN , (int)(0.0125 * screenWidth)));
		addForm.table().getTableHeader().setFont(new Font("SansSerif", Font.BOLD, (int)(0.0126 * screenWidth)));
		
		frame.getContentPane().add(addPanel);
		addPanel.setVisible(false);
		addPanel.setEnabled(false);
		
		
		JPanel searchPanel = new JPanel();
		searchPanel.setBounds((int)(0.005 * screenWidth), (int)(0.005*screenHeight), (int)(0.55 * screenWidth), (int)(0.06*screenHeight));
		frame.getContentPane().add(searchPanel);
		searchPanel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(0, 0,  (int)(0.55 * screenWidth), (int)(0.06*screenHeight));
		textField.setFont(new Font("SansSerif", Font.PLAIN , (int)(0.0125 * screenWidth)));
		searchPanel.add(textField);
		//textField.setColumns(20);
		
		JPanel resultsPanel = new JPanel();
		//resultsPanel.setBounds(10, 87, 1100, 973);
		resultsPanel.setBounds((int)(0.005 * screenWidth), (int)(0.005*2*screenHeight) + (int)(0.06*screenHeight), (int)(0.55 * screenWidth), (int)((1 - (0.005 * 3 + 0.06))*screenHeight));
		frame.getContentPane().add(resultsPanel);
		resultsPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		//scrollPane.setBounds(0, 0, 1100, 890);		
		scrollPane.setBounds(0, 0, (int)(0.55 * screenWidth), (int)(0.86 * screenHeight));
		resultsPanel.add(scrollPane);
		
		JPanel totalPricePanel = new JPanel();
		
		//totalPricePanel.setBounds(1120, 907, 790, 70);
		totalPricePanel.setBounds((int)(0.56 * screenWidth), (int)(0.87 * screenHeight), (int)(0.435 * screenWidth), (int)(0.06 * screenHeight));
		frame.getContentPane().add(totalPricePanel);
		totalPricePanel.setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, (int)(0.015 * screenWidth)));
		textPane.setEditable(false);
		//textPane.setBounds(0, 0, 790, 63);
		textPane.setBounds(0, 0, (int)(0.435 * screenWidth), (int)(0.06 * screenHeight));
		textPane.setText("Tong Cong: 0d");
		DecimalFormat formatter = new DecimalFormat("###,###,###");
		totalPricePanel.add(textPane);
		
		JPanel buttonPanel = new JPanel();
		//buttonPanel.setBounds(1120, 988, 790, 72);
		buttonPanel.setBounds((int)(0.56 * screenWidth), (int)(0.935 * screenHeight), (int)(0.435 * screenWidth), (int)(0.06 * screenHeight));
		frame.getContentPane().add(buttonPanel);
		buttonPanel.setLayout(null);
		
		JButton printBtn = new JButton("In Hóa Đơn");
		printBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
				
				
				
				
				String name =  JOptionPane.showInputDialog("Xin Vui Lòng Nhập Tên Khách Hàng");
				frame.setEnabled(false);
				SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy___hh;mm;ss_a");
				SimpleDateFormat formatter2 = new SimpleDateFormat("dd-M-yyyy hh:mm:ss_a");
				String dateStr = formatter.format(new Date());
				String dateStr2 = formatter2.format(new Date());
				System.out.print(dateStr);
				
		        String fileName = "invoice/" + name + "___" + dateStr + ".docx";
		        String fileName2 = "htmlInvoice/" + name + "___" + dateStr + ".html";
		        String fileName3 = "htmlInvoice/test.html";
        		int columnNumHTML = table_2.getModel().getColumnCount();
        		int rowNumHTML = table_2.getModel().getRowCount();
        		System.out.println(columnNumHTML);
        		System.out.println(rowNumHTML);
        		String[][] dataHTML = new String[rowNumHTML][columnNumHTML];
        		int total = 0;
        		for (int rowIndex = 0; rowIndex < rowNumHTML; rowIndex ++) {
        		    //total = 0;
        		    for (int cellIndex = 0; cellIndex < columnNumHTML; cellIndex ++) {
        		    	dataHTML[rowIndex][cellIndex] = new String(table_2.getModel().getValueAt(rowIndex, cellIndex).toString());
        		    	if (cellIndex == 4) {
        		    		String newValueStr = table_2.getModel().getValueAt(rowIndex, cellIndex).toString().replace(",", "");
        		    		int newValue = Integer.parseInt(newValueStr);
        		    		total = total + newValue;
        		    	}
        		    }
        		}
        		DecimalFormat formatterMoney = new DecimalFormat("###,###,###");
        		String totalStr = formatterMoney.format(total);
        		

				try {


			        
			    Object[] options = {"HTML",
                    "Docx"};
			    int n = JOptionPane.showOptionDialog(null,
			        	"HTML hay Docx?",
			        	"Cách In",
			        	JOptionPane.YES_NO_OPTION,
			        	JOptionPane.QUESTION_MESSAGE,
			        	null,     //do not use a custom Icon
			        	options,  //the titles of buttons
			        	options[0]); //default button title
			        if (n == JOptionPane.YES_OPTION) {
		        		htmlCreator htmlCreator = new htmlCreator(name, dataHTML, totalStr, fileName2, dateStr2);
		        		htmlCreator.createHTML();
				        if (JOptionPane.showConfirmDialog(null, "Bạn Có Muốn In?", null,
				                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				            // yes option
							Desktop.getDesktop().print(new File(fileName2));
				        } else {
				            // no option
				        	Desktop.getDesktop().open(new File(fileName2));
				        }
			        } else {
		        		docxCreator docxCreator = new docxCreator(name, dataHTML, totalStr, fileName, dateStr2);
		        		docxCreator.createDocx();
				        if (JOptionPane.showConfirmDialog(null, "Bạn Có Muốn In?", null,
				                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				            // yes option
							Desktop.getDesktop().print(new File(fileName));
				        } else {
				            // no option
				        	Desktop.getDesktop().open(new File(fileName));
				        }
			        }
				    System.out.println("done");
			        frame.setEnabled(true);
				} catch (Docx4JException e) {
					JOptionPane.showMessageDialog(frame, e);
					frame.setEnabled(true);
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			
			
		});
		//printBtn.setBounds(0, 0, 790, 72);
		printBtn.setBounds(0, 0, (int)(0.435 * screenWidth), (int)(0.06 * screenHeight));
		buttonPanel.add(printBtn);
		
		JPanel cartPanel = new JPanel();
		
		//cartPanel.setBounds(1120, 11, 790, 885);
		cartPanel.setBounds((int)(0.56 * screenWidth), (int)(0.005 * screenHeight), (int)(0.435 * screenWidth), (int)(0.86 * screenHeight));
		frame.getContentPane().add(cartPanel);
		cartPanel.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		//scrollPane_1.setBounds(0, 0, 790, 885);
		scrollPane_1.setBounds(0, 0, (int)(0.435 * screenWidth), (int)(0.86 * screenHeight));
		cartPanel.add(scrollPane_1);
		
		
		DefaultTableModel priceTableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;
			String[] columnNames3 = {"Tên", "SL", "Đ.Giá", "CK", "TT", "DVT"};
			
		    @Override
		    public int getColumnCount() {
		         return columnNames3.length;
		    }

		    @Override
		    public boolean isCellEditable(int row, int col) {
		         return false;
		    }

		    @Override
		    public String getColumnName(int index) {
		        return columnNames3[index];
		    }
			
		};
		
		table_2 = new JTable(priceTableModel);
		table_2.setFont(new Font("SansSerif", Font.PLAIN , (int)(0.0125 * screenWidth)));
		table_2.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, (int)(0.0126 * screenWidth)));
		table_2.setRowHeight(50);
		table_2.getColumnModel().getColumn(0).setPreferredWidth(300);
		table_2.getColumnModel().getColumn(2).setPreferredWidth(150);
		table_2.getColumnModel().getColumn(4).setPreferredWidth(160);
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		table_2.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
		table_2.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
		table_2.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
		table_2.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
		
		scrollPane_1.setViewportView(table_2);
		
		JPanel buttonPanel2 = new JPanel();
		//buttonPanel2.setBounds(0, 901, 1100, 72);
		buttonPanel2.setBounds(0, (int)((0.86 + 0.005) * screenHeight),  (int)(0.55 * screenWidth), (int)(0.06*screenHeight));
		resultsPanel.add(buttonPanel2);
		buttonPanel2.setLayout(null);
		
		JButton addBtn = new JButton("Thoát");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				window.frame.dispose();
				//window = new invoiceForm();	
				//window.frame.setVisible(true);
			}
		});
		addBtn.setBounds(0, 0, 435, 72);
		addBtn.setBounds(0, 0,  (int)(0.2 * screenWidth), (int)(0.06*screenHeight));
		buttonPanel2.add(addBtn);
		
		JButton addBtn2 = new JButton("Thêm");
		addBtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//addForm.setVisible(true);
				resultsPanel.setVisible(false);
				resultsPanel.setEnabled(false);
				addPanel.setVisible(true);
				addPanel.setEnabled(true);
			}
		});
		
		addBtn2.setBounds(665, 0, 435, 72);
		addBtn2.setBounds((int)(0.35 * screenWidth), 0,  (int)(0.2 * screenWidth), (int)(0.06*screenHeight));
		buttonPanel2.add(addBtn2);
		
		try {
			csvParser parser = new itemPropertiesCSV("data/list.csv");
			parser.parseFile();
			items items = parser.listOfItems();				
			
			String[] columnNames2 = {"Mã SP", "Tên", "ĐVT", "Giá", "Giá Bán"};
			Object[][] data2 = items.stocksInObject();
			//System.out.println(data2);
			JTable table_1 = new JTable(data2, columnNames2) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public boolean isCellEditable(int row, int column) {                
	                return false;               
	        }
			};
			
			table_1.setFont(new Font("SansSerif", Font.PLAIN , (int)(0.0125 * screenWidth)));
			table_1.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, (int)(0.0126 * screenWidth)));
			table_1.setRowHeight(50);
			scrollPane.setViewportView(table_1);
			table_1.getColumnModel().getColumn(1).setPreferredWidth((int)(0.12 * screenWidth));

			table_1.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
			table_1.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);

			textField.getDocument().addDocumentListener(new DocumentListener() {
				  public void changedUpdate(DocumentEvent e) {
					  search();
				  }
				  public void removeUpdate(DocumentEvent e) {
					  search();
				  }
				  public void insertUpdate(DocumentEvent e) {
					  search();
				  }

				  public void search() {
					String text = textField.getText();
					Object[][] data3 = items.itemsSearchProject(text);
					DefaultTableModel newModel = new DefaultTableModel(items.itemsSearchProject(text), columnNames2);
					table_1.setModel(newModel);
					((AbstractTableModel) table_1.getModel()).fireTableDataChanged();

				  }
				});
			table_1.addMouseListener(new MouseAdapter() {
			    public void mousePressed(MouseEvent mouseEvent) {
			        JTable table =(JTable) mouseEvent.getSource();
			        Point point = mouseEvent.getPoint();
			        int row = table.rowAtPoint(point);
			        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
			        	int row1 = table_1.getSelectedRow();
			        	String name = table_1.getModel().getValueAt(row1, 1).toString();
			        	String dvt = table_1.getModel().getValueAt(row1, 2).toString();
			        	int quantity =  Integer.parseInt(JOptionPane.showInputDialog("Xin Vui Lòng nhập số lượng."));
			        	int priceEach = Integer.parseInt(table_1.getModel().getValueAt(row1, 4).toString().replace(",",""));
			        	String priceEachStr = formatter.format(priceEach);
			        	int price = (int) (priceEach * (1-0)*quantity);
			        	String priceStr = formatter.format(price);
			        	double discount = Double.parseDouble(JOptionPane.showInputDialog("Xin vui lòng nhập % CK (dưới dạng 0.x)."));
			        	int priceAfterDiscount = (int) (priceEach * (1-discount)*quantity);
			        	String priceAfterDiscountStr = formatter.format(priceAfterDiscount);
			        	//priceTableModel.addRow(new Object[]{name, quantity, price,discount,priceAfterDiscount});
						int rowNum = priceTableModel.getRowCount();
						
						Pattern p = Pattern.compile(name);
					    Matcher m;
					    //Object[][] stocksInObject = new Object[items.size()][5];
					    if (rowNum == 0) {
					    	priceTableModel.addRow(new Object[]{name, quantity, priceEachStr,discount,priceAfterDiscountStr, dvt});
					    }
						for (int i = 0; i < rowNum; i++) {
							m = p.matcher(priceTableModel.getValueAt(i, 0).toString());
							int oldQuantity = (int) priceTableModel.getValueAt(i, 1);
							String oldPriceAfterDiscountStr = priceTableModel.getValueAt(i, 4).toString().replace(",","");
							//int oldPriceAfterDiscount = (int) priceTableModel.getValueAt(i, 4);
							int oldPriceAfterDiscount = Integer.parseInt(oldPriceAfterDiscountStr);
							int newPriceAfterDiscount = priceAfterDiscount + oldPriceAfterDiscount;
							String newPriceAfterDiscountStr = formatter.format(newPriceAfterDiscount);
							
							String oldPriceStr = priceTableModel.getValueAt(i, 2).toString().replace(",","");
							int oldPrice = Integer.parseInt(oldPriceStr);
							int newPrice = price + oldPrice;
							String newPriceStr = formatter.format(newPrice);
							
							if (m.matches()) {
								priceTableModel.setValueAt(quantity + oldQuantity, i, 1);
								priceTableModel.setValueAt(priceEachStr, i, 2);
								priceTableModel.setValueAt(newPriceAfterDiscountStr, i, 4);
								i = 999;
							}
							if (!m.matches() && i == rowNum - 1) {
								priceTableModel.addRow(new Object[]{name, quantity, priceEachStr,discount,priceAfterDiscountStr, dvt});
							}
						}
			        }
			    }
			});
			
			table_2.addMouseListener(new MouseAdapter() {
			    public void mousePressed(MouseEvent mouseEvent) {
			        JTable table =(JTable) mouseEvent.getSource();
			        Point point = mouseEvent.getPoint();
			        int row = table_2.rowAtPoint(point);
			        if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
			        	priceTableModel.removeRow(row);
			        }
			    }
			});
			
			addForm.addBtn().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JTable addFormTable = addForm.table();
					int rowNum = addFormTable.getRowCount();
					boolean match = true;
					for (int i = 0; i < rowNum; i++) {
						String name = (String) addFormTable.getModel().getValueAt(i, 0);
						if (name != null) {
							int quantity =  Integer.parseInt((String) addFormTable.getModel().getValueAt(i, 1));
							String dvt = addFormTable.getModel().getValueAt(i, 2).toString();
				        	int priceEach = Integer.parseInt((String) addFormTable.getModel().getValueAt(i, 3));
				        	String priceEachStr = formatter.format(priceEach);
				        	int price = (int) (priceEach * (1-0)*quantity);
				        	String priceStr = formatter.format(price);
				        	double discount = Double.parseDouble((String) addFormTable.getModel().getValueAt(i, 4));
				        	int priceAfterDiscount = (int) (priceEach * (1-discount)*quantity);
				        	String priceAfterDiscountStr = formatter.format(priceAfterDiscount);
							Pattern p = Pattern.compile(name);
						    Matcher m;

						    int rowNum1 = priceTableModel.getRowCount();
						    
						    if (rowNum1 == 0) {
						    	priceTableModel.addRow(new Object[]{name, quantity, priceStr,discount,priceAfterDiscountStr, dvt});
						    }
							for (int j = 0; j < rowNum1; j++) {
								match = false;
								m = p.matcher(priceTableModel.getValueAt(j, 0).toString());
								int oldQuantity = (int) priceTableModel.getValueAt(j, 1);
								String oldPriceAfterDiscountStr = priceTableModel.getValueAt(j, 4).toString().replace(",","");
								//int oldPriceAfterDiscount = (int) priceTableModel.getValueAt(i, 4);
								int oldPriceAfterDiscount = Integer.parseInt(oldPriceAfterDiscountStr);
								int newPriceAfterDiscount = priceAfterDiscount + oldPriceAfterDiscount;
								String newPriceAfterDiscountStr = formatter.format(newPriceAfterDiscount);
								String oldPriceStr = priceTableModel.getValueAt(j, 2).toString().replace(",","");
								int oldPrice = Integer.parseInt(oldPriceStr);
								int newPrice = price + oldPrice;
								String newPriceStr = formatter.format(newPrice);
								if (m.matches()) {
									priceTableModel.setValueAt(quantity + oldQuantity, j, 1);
									priceTableModel.setValueAt(priceEachStr, j, 2);
									priceTableModel.setValueAt(newPriceAfterDiscountStr, j, 4);
									j = 999;
									match = true;
								}
								else if (!m.matches() && j == rowNum1 - 1 && !match) {
									priceTableModel.addRow(new Object[]{name, quantity, priceEachStr,discount,priceAfterDiscountStr, dvt});
								}
							}
				        	
				        	
						}
					}
				}
			});
			
			priceTableModel.addTableModelListener(new TableModelListener() {

				@Override
				public void tableChanged(TableModelEvent e) {
					// TODO Auto-generated method stub
		        	int totalValue = 0;
		        	int rowNum = priceTableModel.getRowCount();
		        	for (int i = 0; i < rowNum; i++) {
		        		String totalStr = priceTableModel.getValueAt(i, 4).toString().replace(",", "");
		        		int totalInt = Integer.parseInt(totalStr);
		        		//totalValue += (int) priceTableModel.getValueAt(i, 4);
		        		totalValue += totalInt;
		        		
		        	}
		    		
		    		DecimalFormat formatter = new DecimalFormat("###,###,###");
		    		String totalValueString = formatter.format(totalValue);
		    		textPane.setText("Tong Cong: " + totalValueString + "d");
				}
				});
			
			
		} catch (CSVFormatException | DeliveryException | StockException e) {

			e.printStackTrace();
		}
		addForm.backBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addPanel.setVisible(false);
				addPanel.setEnabled(false);
				resultsPanel.setVisible(true);
				resultsPanel.setEnabled(true);
				String[] columnNames3 = {"Tên", "Số Lượng", "ĐVT", "Giá", "CK"};
				Object[][] data = new Object[10][4];
				addForm.table().setModel(new DefaultTableModel(data, columnNames3));
			}
		});

	}
}
