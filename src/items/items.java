package items;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to store a collection of items. This class can be used for representing
 * store inventory, stock orders, sales logs and truck cargo.
 * 
 * @author Minh
 *
 */
public class items {
	private ArrayList<item> items;
	private items stock;
	
	/**
	 * Constructor to create a new class object to store items. This constructor initializes
	 * a an empty array list to store Items.
	 */
	public items () {
		items = new ArrayList<item>();
	}
	
	/**
	 * This method add an item into the list. This method takes an item as input
	 * then add that item to the list.
	 * 
	 * @param item needs to be added
	 */
	public void addItem(item item) {
		items.add(item);
	}

	/**
	 * This method remove an item from the list at input index. This method takes
	 * an item index as input then removes the item at that index from the list.
	 * @param index of the item needs to be removed
	 */
	public void remove(int index) {
		items.remove(index);
	}
	
	/**
	 * THis methods return an item from the list at the value of the input index position.
	 * This method takes an index as input
	 * 
	 * @param index of the needed item
	 * @return an item at the input index
	 */
	public item getItem(int index) {
		return items.get(index);
	}
	
	/**
	 * This method returns how many items in the list, which
	 * is the number of distinct items in the list
	 * 
	 * @return the number of distinct items in the list
	 */
	public int getSize() {
		return items.size();
		
	}
	
	/**
	 * This method returns a whole list of the items
	 * 
	 * @return list of items
	 */
	public ArrayList<item> stocks () {
		return items;
	}
	
	/**
	 * This method returns total quantity of items in the list. This
	 * method loops through every item in the list and get that item's quantity.
	 * The method then add that item's quantity to the totalQuantity variable.
	 * 
	 * @return total quantity of items in the list
	 */
	//public int totalQuantity() {
	//	int totalQuantity = 0;
	//	for (item item : items) {
	//		totalQuantity += item.quantity();
	//	}
	//	return totalQuantity;
	//}
	
	/**
	 * This method returns the total cost of the list. THis method
	 * loops through every item in the list, get that item's quantity 
	 * and manufacturing cost. The method then multiply item's quantity 
	 * with its manufacturing cost, then add the result to the totalCost
	 * variable.
	 * 
	 * @return total cost of the list
	 */
	//public double totalCost() {
	//	int totalCost = 0;
	//	for (item item : items) {
	//		totalCost += item.quantity() * item.manufacturingCost();
	//	}
	//	return totalCost;
	//}
	
	/**
	 * This method returns the total sell price of the list. THis method
	 * calculate the total sell price of each item by multiplying that item's
	 * quantity with its sell price, then add those sell price together 
	 * @return total sell price of the list
	 */
	//public double totalSell() {
	//	double totalSell = 0;
	//	for (item item : items) {
	//		totalSell += item.quantity() * item.sellPrice();
	//	}
	//	return totalSell;
	//}
	
	/**
	 * THis method returns the position of an item which has a name that matches
	 * the input. This loop through every item in the list and compares that item's name
	 * with the input name. The method returns the position if matches and -1 if 
	 * nothing matches
	 * 
	 * @param name of the item needs to be searched
	 * @return position if found and -1 if not found
	 */
	public int itemPosition(String name) {
		int counter = 0;
		for (item item : items) {
			if (item.name().equals(name)) {
				return counter; // found the item that matches the input
			}
			counter += 1;
		}
		return -1; // not found
	}
	
	/**
	 * This method returns the list of item in the format of an 2D array of objects.
	 * @return list of item in 2D array format
	 */
	public Object[][] stocksInObject() {
		Object[][] stocksInObject = new Object[items.size()][5];
		item item;
		DecimalFormat formatterMoney = new DecimalFormat("###,###,###");
		for (int i = 0; i < items.size(); i ++) {
			item = items.get(i);
			stocksInObject[i][0] = new String(item.id());
			stocksInObject[i][1] = new String(item.name());
			stocksInObject[i][2] = new String(item.quantity());
			stocksInObject[i][3] = new String(formatterMoney.format(item.manufacturingCost()));
			stocksInObject[i][4] = new String(formatterMoney.format(item.sellPrice()));
		}
		return stocksInObject;
	}
	
	/**
	 * This method returns the list of item's name
	 */
	public String[] stocksText() {
		String[] stocksText = new String[this.getSize()];
		for (int i = 0; i < items.size(); i ++) {
			item item = items.get(i);
			stocksText[i] = new String(item.id() + "       |       " + item.name() + "       |       " + item.sellPrice());
		}
		return stocksText;
	}
	
	/**
	 * THis method search
	 */
	
	public String[] itemsSearch(String text) {
	    Pattern p = Pattern.compile(text);
	    Matcher m;
	    String[] names = new String[this.getSize()];
	    for (int i = 0; i < this.getSize(); i++) {
	    	item item = items.get(i);
	    	m = p.matcher(item.id());
	    	if (m.find()) {
	    		System.out.println("yes");
	    		names[i] = new String(item.id() + "       |       " + item.name() + "       |       " + item.sellPrice());
	    	}
	    }
	    return names;
	}
	
	/**
	 * THis method search
	 */
	
	public Object[][] itemsSearchProject(String text) {
	    Pattern p = Pattern.compile(text);
	    Matcher m;
	    //Object[][] stocksInObject = new Object[items.size()][5];
	    ArrayList<Object> stockList = new ArrayList<Object>();
	    int index = 0;
	    DecimalFormat formatterMoney = new DecimalFormat("###,###,###");
	    for (int i = 0; i < this.getSize(); i++) {
	    	item item = items.get(i);
	    	Object[] items = new Object[5];
	    	m = p.matcher(item.id());
	    	if (m.find()) {
				//stocksInObject[index][0] = new String(item.id());
				//stocksInObject[index][1] = new String(item.name());
				//stocksInObject[index][2] = new Integer(item.quantity());
				//stocksInObject[index][3] = new Integer(item.manufacturingCost());
				//stocksInObject[index][4] = new Integer(item.sellPrice());
				items[0] = new String(item.id());
				items[1] = new String(item.name());
				items[2] = new Integer(item.quantity());
				items[3] = new Integer(item.manufacturingCost());
				items[4] = new Integer(item.sellPrice());
				stockList.add(items);
				//index += 1;
	    	}
	    }
	    Object[][] stocksInObject = stockList.toArray(new Object[stockList.size()][5]);
	    return stocksInObject;
	}
}
