package parser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.apache.poi.hpsf.Decimal;


public class csvCreator {
    public static void main(String[]args) throws FileNotFoundException{
        PrintWriter pw = new PrintWriter(new File("test.csv"));
        StringBuilder sb = new StringBuilder();
        String[] name = {"1.5", "2.5", "3.5", "4.0", "6.0", "8.0", "10.0", "11.0", "2.16", "2.24", "2.32", "2.50", "2.30", "2.0.75", "2.1.0", "2.1.5", "2.2.5", "2.4", "2.6"};
        String[] price = {"4136","6732", "8921", "10197", "14982", "19789", "24530", "26180", "3036", "4279", "5489", "12683", "7821", "5038", "6303", "8888", "14311", "21637", "32340"};
        for (int i = 0; i < name.length; i++) {
            sb.append("dmcv"+name[i]);
            sb.append(',');
            sb.append("Cap Cadivi CV " + name[i]);
            sb.append(',');
            sb.append("met");
            sb.append(',');
            sb.append(price[i]);
            sb.append('\n');
        }

        pw.write(sb.toString());
        pw.close();
        System.out.println("done!");
    }
}