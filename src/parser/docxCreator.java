package parser;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;

import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.MarginsWellKnown;
import org.docx4j.model.structure.PageDimensions;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.model.table.TableModel;
import org.docx4j.model.table.TblFactory;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTTblCellMar;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.docx4j.wml.PPrBase.Spacing;
import org.docx4j.wml.SectPr.PgMar;

public class docxCreator {
	private String[][] data;
	private String total;
	private String fileName;
	private String name;
	private String dateStr;
	
	public docxCreator(String name, String[][] data, String total, String fileName, String dataStr) {
		this.name = name;
		this.data = data;
		this.total = total;
		this.fileName = fileName;
		this.dateStr = dataStr;
	}
	
	public void createDocx() throws Docx4JException {
        WordprocessingMLPackage wordPackage;
	        
	        File doc = new File("data/invoice2.docx");
	        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage
	          .load(doc);
	        MainDocumentPart mainDocumentPart1 = wordMLPackage
	          .getMainDocumentPart();
	        ArrayList<Object> datas = (ArrayList<Object>) mainDocumentPart1.getContent();
	        System.out.println(datas);
	        
			wordPackage = WordprocessingMLPackage.createPackage();
	        MainDocumentPart mainDocumentPart = wordPackage.getMainDocumentPart();

	        File exportFile = new File(fileName);	        
	        for (Object obj : datas) {
	        	mainDocumentPart.getContent().add(obj);
	        }
	        BigInteger bignumber = mainDocumentPart.getJaxbElement().getBody().getSectPr().getPgMar().getLeft();
	        Double numberInCm = (bignumber.intValue() / 20 / 72 * 2.54);
	        System.out.println(numberInCm);
	        PgMar pageMargin = new PgMar();
	        
	        PageDimensions pageDimensions = new PageDimensions();
	        
	        pageDimensions.setPgSize(PageSizePaper.A5, false);
	        mainDocumentPart.getJaxbElement().getBody().getSectPr().setPgSz(pageDimensions.getPgSz());
	        pageDimensions.setMargins(MarginsWellKnown.NARROW);
	        mainDocumentPart.getJaxbElement().getBody().getSectPr().setPgMar(pageDimensions.getPgMar());
	        
	        
	        wordPackage.save(exportFile);
	        ObjectFactory factory = Context.getWmlObjectFactory();    
	        BooleanDefaultTrue b = new BooleanDefaultTrue();
	        P p = factory.createP();
	        R r = factory.createR();
	        Text t = factory.createText();
	        P p2 = factory.createP();
	        R r2 = factory.createR();
	        Text t2 = factory.createText();
	        t.setValue("Tên Khách Hàng: " + name);
	        r.getContent().add(t);
	        p.getContent().add(r);
	        t2.setValue("Ngày: " + dateStr);
	        r2.getContent().add(t2);
	        p2.getContent().add(r2);
	        RPr rpr = factory.createRPr();
	        RFonts CambriaFont = new RFonts();
	        CambriaFont.setAscii("Cambria");
	        rpr.setRFonts(CambriaFont);
	        
	        
	        PPr pppr = factory.createPPr();
	        
	        Spacing spacing = new Spacing();
	        spacing.setAfter(BigInteger.valueOf(0));
	        pppr.setSpacing(spacing);
	        
	        p.setPPr(pppr);
	        p2.setPPr(pppr);
	        
	        
	        r.setRPr(rpr);
	        r2.setRPr(rpr);
	        mainDocumentPart.getContent().add(p);
	        mainDocumentPart.getContent().add(p2);
	        
	        wordPackage.save(exportFile);
	        
			String[] columnNamesInvoice = {"Tên", "SL", "Đ.Giá", "CK", "TT (sau CK)"};
	        
	        int writableWidthTwips = wordPackage.getDocumentModel()
	        		  .getSections().get(0).getPageDimensions().getWritableWidthTwips();
	        		int columnNum = data[0].length;
	        		int rowNum = data.length + 2;
	        		
	        		Tbl tbl = TblFactory.createTable(rowNum, columnNum, writableWidthTwips/columnNum);   
	        		ArrayList<Object> rows = (ArrayList<Object>) tbl.getContent();
	        		for (int rowIndex = 0; rowIndex < rows.size(); rowIndex ++) {
	        			Tr tr = (Tr) rows.get(rowIndex);
	        		    ArrayList<Object> cells = (ArrayList<Object>) tr.getContent();     		    
	        		    for (int cellIndex = 0; cellIndex < cells.size(); cellIndex ++) {

	        		    	Tc td = (Tc) cells.get(cellIndex);
	    			        P p1 = factory.createP();
	    			        R r1 = factory.createR();
	    			        Text t1 = factory.createText();
	    			        String text = null;
        			        HpsMeasure size = new HpsMeasure();
        			        size.setVal(BigInteger.valueOf(22));
        			        
	        		    	if (rowIndex == 0) {
	        		    		RPr rpr1 = factory.createRPr();
	        			        rpr1.setB(b);
	        			        rpr1.setI(b);
	        			        rpr1.setCaps(b);
	        			        rpr1.setSz(size);
	        			        rpr1.setRFonts(CambriaFont);
	        			        r1.setRPr(rpr1);
	        		    		text = columnNamesInvoice[cellIndex];
	        		    	} else if (rowIndex == rowNum - 1) { 
	        		    		if (cellIndex == 2) {
		        		    		RPr rpr1 = factory.createRPr();
		        			        rpr1.setB(b);
		        			        rpr1.setI(b);
		        			        rpr1.setCaps(b);
		        			        rpr1.setSz(size);
		        			        rpr1.setRFonts(CambriaFont);
		        			        r1.setRPr(rpr1);
	        		    			text = "Tong Cong: ";
	        		    		} else if (cellIndex == 4) {		     
	        		    			RPr rpr1 = factory.createRPr();
	        		    			text = total;
	        		    			rpr1.setRFonts(CambriaFont);
		        			        r1.setRPr(rpr1);
	        		    		}
	        		    	}
	        		    	else {
	        		    		RPr rpr1 = factory.createRPr();
	        		    		size.setVal(BigInteger.valueOf(20));
	        		    		rpr1.setSz(size);
	        			        rpr1.setRFonts(CambriaFont);
	        			        r1.setRPr(rpr1);
		        		    	text = data[rowIndex - 1][cellIndex];
	        		    		if (cellIndex == 2 || cellIndex == 4) {
	        		    		}
	        		    		
	        		    	}
	        		    	
	    			        t1.setValue(text);
	    			        r1.getContent().add(t1);
	    			        p1.getContent().add(r1);
	    			        
	    			        if (cellIndex > 0 && rowIndex > 0) {
		    			        PPr ppr = factory.createPPr();
		    			        Jc testJc = factory.createJc();
		    			        
		    			        testJc.setVal(JcEnumeration.RIGHT);
		    			        ppr.setJc(testJc);
		    			        
		    			        p1.setPPr(ppr);
		    			        
	    			        }
	    			        if (rowIndex == 0) {
	    			        	PPr ppr = factory.createPPr();
		    			        Jc testJc = factory.createJc();
		    			        
		    			        testJc.setVal(JcEnumeration.CENTER);
		    			        ppr.setJc(testJc);
		    			        
		    			        p1.setPPr(ppr);
		    			        
	    			        }

	    			        
	        		    	td.getContent().set(0, p1);
	        		    	
	        		    	if (cellIndex == 0) {
		        		    	TblWidth cellW = new TblWidth();
		        		    	cellW.setW(BigInteger.valueOf(2500));
		        		    	td.getTcPr().setTcW(cellW);
	        		    	}
	        		    	
	        		    	TblWidth cellW = new TblWidth();
	        		    	switch (cellIndex) {
	        		    	case 0:
	        		    		cellW.setW(BigInteger.valueOf(3000));
		        		    	td.getTcPr().setTcW(cellW);
	        		    		break;
	        		    	case 1:
	        		    		cellW.setW(BigInteger.valueOf(1000));
		        		    	td.getTcPr().setTcW(cellW);
	        		    		break;
	        		    	case 2:
	        		    		cellW.setW(BigInteger.valueOf(1500));
		        		    	td.getTcPr().setTcW(cellW);
	        		    		break;
	        		    	case 3:
	        		    		cellW.setW(BigInteger.valueOf(1000));
		        		    	td.getTcPr().setTcW(cellW);
	        		    		break;
	        		    	case 4:
	        		    		cellW.setW(BigInteger.valueOf(1500));
		        		    	td.getTcPr().setTcW(cellW);
	        		    		break;
	        		    	}

	        		    }
	        		}
	        TableModel tblModel = new TableModel();
	        TblPr tablePr = tbl.getTblPr();
	        CTTblCellMar cellMarginValue = new CTTblCellMar();
	        TblWidth tableWidth = new TblWidth();
	        
	        tableWidth.setW(BigInteger.valueOf(50));
	        cellMarginValue.setLeft(tableWidth);
	        cellMarginValue.setRight(tableWidth);
	        cellMarginValue.setTop(tableWidth);
	        cellMarginValue.setBottom(tableWidth);
	        tablePr.setTblCellMar(cellMarginValue);
	        tbl.setTblPr(tablePr);
	        
	        
	        mainDocumentPart.getContent().add(tbl);
	        wordPackage.save(exportFile);}
}
