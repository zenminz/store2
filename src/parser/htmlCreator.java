package parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class htmlCreator {
	private String[][] data;
	private String total;
	private String fileName;
	private String name;
	private String dateStr;
	
	public htmlCreator(String name, String[][] data, String total, String fileName, String dataStr) {
		this.name = name;
		this.data = data;
		this.total = total;
		this.fileName = fileName;
		this.dateStr = dataStr;
	}
	
	public void createHTML() {
		File htmlFIle = new File(fileName);
		String html  = "<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<script src=\"https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.js\"></script>\r\n" + 
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"semantic.css\">" + 
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"mycss.css\">\r\n" + 


				"<body>\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"<div class= \"ui equal width grid\">\r\n" + 
				"    <div class=\"column\">\r\n" + 
				"    	<h2> TIỆM ĐIỆN A CÓN </h2>\r\n" + 
				"		<p>ĐC: 116 ĐƯỜNG SỐ 8, KV. THẠNH THUẬN, P. PHÚ THỨ, Q. CÁI RĂNG, TPCT\r\n" + 
				"			<br>\r\n" + 
				"			ĐT: 0292.3917113 – DĐ: 0939 679 585\r\n" + 
				"			<br>\r\n" + 
				"			MST: 8055104059\r\n" + 
				"		</p>\r\n" + 
				"    </div>\r\n" + 
				"    <div class=\"column\">\r\n" + 
				"      <p><img src=\"logo.png\" alt=\"logo\" height=\"150\" width=\"170\"></p>\r\n" + 
				"      <p></p>\r\n" + 
				"      <p></p>\r\n" + 
				"      <p></p>\r\n" + 
				"    </div>\r\n" + 
				"</div>\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"<h1>Hóa Đơn Bán Hàng</h1>\r\n" + 
				"<h3> Tên Khách Hàng: " + name + "<br> Ngày: " + dateStr + "</h3>\r\n";
		
		String tableHtml = "<table cellspacing=\"5\" class=\"ui celled table\">\r\n" + 
				"  <thead>\r\n" + 
				"    <tr>\r\n" + 
				"    	<th class=\"center aligned\">Tên</th>\r\n" + 
				"    	<th class=\"center aligned\">SL</th>\r\n" + 
				"    	<th class=\"center aligned\">ĐVT</th>\r\n" + 
				"    	<th class=\"center aligned\">Đ.Giá</th>\r\n" + 
				"    	<th class=\"center aligned\">CK</th>\r\n" + 
				"    	<th class=\"center aligned\">TT sau CK</th>\r\n" + 
				"  	</tr>\r\n" + 
				"  </thead>\r\n" + 
				"  <tbody>";
		
		int rowNum = data.length;
		//int columnNum = data[0].length;
		for (int rowIndex = 0; rowIndex < rowNum; rowIndex ++) {
				tableHtml += "<tr>\r\n" + 
						"      <td data-label=\"Tên\">" + data[rowIndex][0] + "</td>\r\n" + 
						"      <td class=\"right aligned\" data-label=\"SL\">" + data[rowIndex][1] + "</td>\r\n" + 
						"      <td class=\"right aligned\" data-label=\"ĐVT\">" + data[rowIndex][5] + "</td>\r\n" + 
						"      <td class=\"right aligned\" data-label=\"Đ.Giá\">" + data[rowIndex][2] + "</td>\r\n" + 
						"      <td class=\"right aligned\" data-label=\"CK\">" + data[rowIndex][3] + "</td>\r\n" + 
						"      <td class=\"right aligned\" data-label=\"TT Sau CK\">" + data[rowIndex][4] + "</td>\r\n" + 
						"    </tr>";
		}
		
		tableHtml += "<tr>\r\n" + 
				"      <td data-label=\"Tên\"></td>\r\n" + 
				"      <td class=\"right aligned\" data-label=\"SL\"></td>\r\n" + 
				"      <td class=\"right aligned\" data-label=\"ĐVT\"></td>\r\n" + 
				"      <td class=\"center aligned\" data-label=\"Đ.Giá\">Tổng Cộng</td>\r\n" + 
				"      <td class=\"right aligned\" data-label=\"CK\"></td>\r\n" + 
				"      <td class=\"right aligned\" data-label=\"TT Sau CK\">" + total + "</td>\r\n" + 
				"    </tr>";
		tableHtml += "  </tbody>\r\n" + 
				"</table>";
		
		html += tableHtml;
		html += "</body>\r\n" + 
				"</html>";
	    BufferedWriter writer;
		try {
			writer =  new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
			 writer.write(html);
			 writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	     
	   
		
	}
}
