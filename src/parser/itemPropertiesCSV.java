package parser;

import java.io.IOException;
import java.util.ArrayList;
import Exception.CSVFormatException;
import items.item;
import items.items;

import java.io.*;

/**
 * This is subclass of the ParseCSV class. This subclass parses the data string from the item properties csv 
 * and puts those data in a Stocks object.
 * @author Minh
 *
 */
public class itemPropertiesCSV extends csvParser {
	ArrayList<String> textFromFile;
	items itemPropertiesList;
	
	/**
	 * Constructor for initializing the file of a itemPropertiesCSV file, which is required to be parsed.
	 * This constructor also tries to read the file and stores the data of that file into 
	 * a string array
	 * 
	 * @param fileName of the file needs to be read and parsed
	 * @throws CSVFormatException when the file is not a csv file
	 */
	public itemPropertiesCSV(String fileName) throws CSVFormatException{
		super(fileName);

	}

	@Override
	public void parseFile() throws CSVFormatException {

		try {
			textFromFile = readFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		boolean correct = true;
		String[][] items_properties = new String[textFromFile.size()][6];
		
		if (correct) {
			for (int i = 0; i < textFromFile.size(); i++) {
				items_properties[i] = textFromFile.get(i).split(","); 				
			}
			
			
			itemPropertiesList = new items();
			item item;
			String id;
			String name;
			int manufacturingCost;
			int sellingPrice;
			String quantity;
			
			for (int i = 1; i < items_properties.length; i ++) {
				id = items_properties[i][0];
				name = items_properties[i][1];
				manufacturingCost = Integer.parseInt(items_properties[i][2]);
				sellingPrice = Integer.parseInt(items_properties[i][3]);
				quantity = items_properties[i][4];
				item = new item(id, name, manufacturingCost, sellingPrice, quantity);
				itemPropertiesList.addItem(item);
			}
		} else {
			throw new CSVFormatException("not item properties");
		}
		
	}

	@Override
	public items listOfItems() {
		return itemPropertiesList;
	}

}
